pipeline {
    agent any
    parameters {
        booleanParam(name: "TEST_BOOLEAN", defaultValue: true, description: "Sample boolean parameter")
        string(name: "TEST_STRING", defaultValue: "ssbostan", trim: true, description: "Sample string parameter")
        text(name: "TEST_TEXT", defaultValue: "Jenkins Pipeline Tutorial", description: "Sample multi-line text parameter")
        password(name: "TEST_PASSWORD", defaultValue: "SECRET", description: "Sample password parameter")
        choice(name: "TEST_CHOICE", choices: ["production", "staging", "development"], description: "Sample multi-choice parameter")
    }
    stages {
        stage("Build") {
            steps {
                echo "Build stage."
                echo "Hello $params.TEST_STRING"
            }
        }
        stage("Test") {
            steps {
                echo "Test stage."
            }
        }
        stage("Release") {
            steps {
                echo "Release stage."
            }
        }
    }
}
################
pipeline {
    agent any
    environment {
        SAMPLE_GLOBAL_ENV_VAR = "Test global ENV variables."
    }
    stages {
        stage("Build") {
            environment {
                SAMPLE_STAGE_ENV_VAR = "Test stage ENV variables."
            }
            steps {
                echo "Build stage."
                echo "$SAMPLE_GLOBAL_ENV_VAR"
                echo "$SAMPLE_STAGE_ENV_VAR"
            }
        }
        stage("Test") {
            steps {
                echo "Test stage."
                echo "$env.SAMPLE_GLOBAL_ENV_VAR"
            }
        }
        stage("Release") {
            steps {
                echo "Release stage."
                echo "${SAMPLE_GLOBAL_ENV_VAR}"
            }
        }
    }
}